require 'json'

File.open("./input.txt", 'r').readlines.map do |line|
  term, definition = line.split(" - ")
  response = { term: term }

  rad = definition.match(/\(radical:\s.*[–-]\)/)
  if rad.nil?
    response[:radical] = ''
  else
    response[:radical] = rad[0].split(': ').last[0..-2]
  end

  contrato = definition.match(/\(contrato\sem\s.*\)/)
  if contrato.nil?
    response[:contrato] = ''
  else
    response[:contrato] = contrato[0].split(' em ').last[0..-2]
  end

  if definition.include?('irregular')
    response[:conjugacao] = 'irregular'
  elsif definition.include?('regular')
    response[:conjugacao] = 'regular'
  end

  if definition.include?('média')
    response[:voz] = 'média'
  elsif definition.include?('depoente')
    response[:voz] = 'depoente'
  end

  dec = definition.match(/[123][abcd]/)
  if dec.nil?
    response[:declinacao] = ''
  else
    response[:declinacao] = dec[0]
  end

  if definition.include?('(+ ')
    tok = definition.split(" (")
    response[:definicao] = "#{tok[0]} (#{tok[1]}"
  else
    response[:definicao] = definition.split(" (")[0]
  end

  filename = response[:term].gsub(/\(.*\)\s/, '')
  File.open("./output/#{filename}.json", "w+") do |f|
    f.write(JSON.generate(response))
  end
end
