const path = require('path')

export default {
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap' }
    ]
  },

  content: {
    fullTextSearchFields: ['word', 'alternate', 'definitions.text'],
    nestedProperties: ['definitions.class', 'definitions.text']
  },

  css: [
  ],

  styleResources: {
    scss: [
      './assets/tokens/*.scss',
    ],
  },

  plugins: [
  ],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    // '@nuxtjs/stylelint-module',
    '@nuxtjs/style-resources'
  ],

  modules: [
    '@nuxt/content'
  ],

  content: {},

  build: {
    extend (config, context) {
      config.resolve.alias['@'] = path.resolve(__dirname)
    }
  }
}
